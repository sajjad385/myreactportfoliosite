import React,{Component} from 'react';
import {Cell, Grid} from "react-mdl";

class Training extends Component{

    render(){
        return (
            <div>
                <Grid>
                    <Cell col={4}>

                        <p  style={{marginTop:'0px', marginBottom:'0px'}}>{this.props.startYear}  -  {this.props.endYear}</p>
                        <p  style={{marginTop:'0px', marginBottom:'0px'}}>{this.props.instituteName}</p>

                    </Cell>
                    <Cell col={8}>
                        <h4 style={{marginTop:'0px'}}>
                            {this.props.trainingName}
                        </h4>
                        <p>{this.props.trainingDescriptions}</p>
                    </Cell>
                </Grid>
            </div>

        );
    }

}
export default Training;

