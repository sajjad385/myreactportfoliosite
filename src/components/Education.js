import React,{Component} from 'react';
import {Cell, Grid} from "react-mdl";

class Education extends Component{

    render(){
        return (
            <div>
                <Grid>
                    <Cell col={4}>

                        <p  style={{marginTop:'0px', marginBottom:'0px'}}>{this.props.startYear}  -  {this.props.endYear}</p>
                        <p  style={{marginTop:'0px', marginBottom:'0px'}}>{this.props.subject}</p>
                        <h5 style={{marginTop:'0px', marginBottom:'0px'}}>{this.props.degree}</h5>

                    </Cell>
                    <Cell col={8}>
                        <h4 style={{marginTop:'0px'}}>
                            {this.props.schoolName}
                        </h4>
                        <p>{this.props.schoolDetails}</p>
                    </Cell>
                </Grid>
            </div>

        );
    }

}
export default Education;

