import React, {Component} from 'react';
import {Cell, Grid, List, ListItem, ListItemContent} from "react-mdl";

class Contact extends Component {

    render() {
        return (
            <div className="contact-body">
                <Grid className="contact-grid">
                    <Cell col={6}>
                        <h4>Mohammad Sajjad Hossain</h4>
                        <img src="https://gitlab.com/uploads/-/system/user/avatar/2035473/avatar.png" alt="avator"
                             style={{height: '250px', borderRadius:'50%'}}/>
                        <p>
                            It’s my pleasure to introduce myself. This is Mohammad Sajjad Hossain. I completed my
                            diploma from the department of CSE from Bangladesh Sweden Polytechnic Institute.Currently  studying BSc in CSE at Green University of Bangladesh (GUB). Now I'm work as
                            Software Engineer at Shonod Edutainment .
                        </p>
                    </Cell>
                    <Cell col={6}>
                        <h2>Contact Me</h2>
                        <hr/>
                        <div className="contact-list">
                            <List>
                                <ListItem>
                                    <ListItemContent icon="phone">+88 018 6713 1561</ListItemContent>
                                </ListItem>
                                <ListItem>
                                    <ListItemContent icon="email">cse.sajjad.hossain@gmail.com</ListItemContent>
                                </ListItem>
                                <ListItem>
                                    <ListItemContent icon="sms">www.facebook.com/sorad85</ListItemContent>
                                </ListItem>
                                <ListItem>
                                    <ListItemContent icon="sms">live:mohammadsajjad585(skype)</ListItemContent>
                                </ListItem>
                                <ListItem>
                                    <ListItemContent  icon="map">Niketon Bazar, Road No -08, Dhaka-1212</ListItemContent>
                                </ListItem>
                            </List>
                        </div>

                    </Cell>
                </Grid>
            </div>

        );
    }

}

export default Contact;

