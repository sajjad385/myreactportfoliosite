import React, {Component} from 'react';
import {Button, Card, CardActions, CardMenu, CardText, CardTitle, Cell, Grid, IconButton, Tab, Tabs} from "react-mdl";
import {Link} from "react-router-dom";

class Project extends Component {
    constructor(props) {
        super(props);

        this.state = {activeTab: 0}
    }

    toggleCategories() {
        if (this.state.activeTab === 0) {
            return (
                <Grid className="demo-grid-ruler">
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://thumbnails.webinfcdn.net/thumbnails/350x350/d/digitalstudyroom.com.png) center / cover'
                            }}>Digital Studyroom</CardTitle>
                            <CardText>
                                Digital Studyroom - Truly largest digital edu- cation website in Bangladesh. This is
                                online
                                education platform for students studying in Bangladesh. It provides tutorials for
                                students
                                based on the Bangladesh
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="http://www.digitalstudyroom.com/" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://pic.accessify.com/thumbnails/777x423/s/shonod.com.png) center / cover'
                            }}>Shonod Edutainment</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eveniet ex vero?
                                Ab eius
                                enim eos, facere fugiat illo libero maxime modi nemo nostrum sint vero. Aperiam cum
                                perspiciatis sunt?
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="https://shonodedu.netlify.app/" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://i.ytimg.com/vi/K4gjmoKTEAc/maxresdefault.jpg) center / cover'
                            }}>Learn Programming BD</CardTitle>
                            <CardText>
                                This a Programming Blog web application using Laravel Framework.
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequuntur
                                dolores
                                earum exercitationem hic impedit inventore magnam.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="http://learnprogrammingbd.cf/" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>

                    </Cell>
                </Grid>
            )
        }
        else if (this.state.activeTab === 1) {
            return (
                <Grid className="demo-grid-ruler">
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://plint-sites.nl/images/expertise/react.jpg) center / cover'
                            }}>MSHS- Personal Portfolio</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum
                                facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum
                                quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="https://csesajjad.netlify.app" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://plint-sites.nl/images/expertise/react.jpg) center / cover'
                            }}>MSHS- Personal Portfolio</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum
                                facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum
                                quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="https://csesajjad.netlify.app" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://plint-sites.nl/images/expertise/react.jpg) center / cover'
                            }}>MSHS- Personal Portfolio</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum
                                facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum
                                quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="https://csesajjad.netlify.app" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                </Grid>
            )
        }
        else if (this.state.activeTab === 2) {
            return (
                <Grid className="demo-grid-ruler">
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://learnworthy.net/wp-content/uploads/2020/03/PHP.jpg) center / cover'
                            }}>PHP Project #1</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum
                                facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum
                                quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><Link to="/">Live
                                    Demo </Link></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://learnworthy.net/wp-content/uploads/2020/03/PHP.jpg) center / cover'
                            }}>PHP Project #1</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum
                                facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum
                                quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><Link to="/">Live
                                    Demo </Link></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://learnworthy.net/wp-content/uploads/2020/03/PHP.jpg) center / cover'
                            }}>PHP Project #1</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum
                                facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum
                                quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><Link to="/">Live
                                    Demo </Link></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                </Grid>
            )
        }
        else if (this.state.activeTab === 3) {
            return (
                <Grid className="demo-grid-ruler">
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://www.technotification.com/wp-content/uploads/2019/01/Dart-programming-language.jpg) center / cover'
                            }}>COVID-19-TRACKER </CardTitle>
                            <CardText>
                                Coronaviruses are a large famliy of viruses which may cause illness in animals or humans.
								Covid-19 is the infectious disease caused by the most recently discovered coronovirus.
                            </CardText>
                            <CardActions border>
                                <Button colored><a href="https://github.com/sajjad385/covid-19-tracker" target="_blank"
                                                   rel="noopener noreferrer">Github</a></Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored>
                                    <a href="#live-demo"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://www.technotification.com/wp-content/uploads/2019/01/Dart-programming-language.jpg) center / cover'
                            }}>NAMAJ SHIKKA </CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor
                                dolorem eum
                                facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum
                                quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>
								<a href="https://github.com/sajjad385/namajShikka" target="_blank"
                                                   rel="noopener noreferrer">Github</a>
								</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="#live-demo" rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://www.technotification.com/wp-content/uploads/2019/01/Dart-programming-language.jpg) center / cover'
                            }}>Food Delivery App</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor
                                dolorem eum
                                facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum
                                quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>
								<a href="https://github.com/sajjad385/food_delivery_app" target="_blank"
                                                   rel="noopener noreferrer">Github</a>
								</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="#live-demo" rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                </Grid>
            )
        }
        else if (this.state.activeTab === 4) {
            return (
                <Grid className="demo-grid-ruler">
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQu8NO_IHlCNzy_NlMKTbEHjFr-QZxVdW-BKLSfdfJ06TIF7-9q&usqp=CAU) center / cover'
                            }}>JavaScript Project #1</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="https://csesajjad.netlify.app" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQu8NO_IHlCNzy_NlMKTbEHjFr-QZxVdW-BKLSfdfJ06TIF7-9q&usqp=CAU) center / cover'
                            }}>JavaScript Project #1</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="https://csesajjad.netlify.app" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                    <Cell col={4}>
                        <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                            <CardTitle style={{
                                color: '#000000',
                                height: '176px',
                                background: 'url(https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQu8NO_IHlCNzy_NlMKTbEHjFr-QZxVdW-BKLSfdfJ06TIF7-9q&usqp=CAU) center / cover'
                            }}>JavaScript Project #1</CardTitle>
                            <CardText>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequatur dolor dolorem
                                eum facilis fugit illum incidunt iusto, laborum mollitia natus necessitatibus nesciunt
                                nostrum quas, quod similique tempora temporibus voluptate.
                            </CardText>
                            <CardActions border>
                                <Button colored>Gitlab</Button>
                                <Button colored>LinkedIn</Button>
                                <Button colored><a href="https://csesajjad.netlify.app" target="_blank"
                                                   rel="noopener noreferrer">Live
                                    Demo </a></Button>
                            </CardActions>
                            <CardMenu style={{color: '#fff'}}>
                                <IconButton name="share"/>
                            </CardMenu>
                        </Card>
                    </Cell>
                </Grid>
            )
        }
    }

    render() {
        return (
            <div className='category-tabs'>
                <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({activeTab: tabId})} ripple>
                    <Tab>Laravel</Tab>
                    <Tab>React</Tab>
                    <Tab>PHP</Tab>
                    <Tab>Dart</Tab>
                    <Tab>JavaSrcipt</Tab>
                </Tabs>
                <div style={{width: '100%', margin: 'auto'}}>
                    {this.toggleCategories()}
                </div>
            </div>

        );
    }

}

export default Project;

