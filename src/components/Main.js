import React from 'react';
import {Route, Switch} from "react-router-dom";
import Home from './Home'
import Resume from "./Resume";
import Project from "./Project";
import Contact from "./Contact";


const Main =()=>(
    <Switch>
        <Route exact path ="/" component={Home}/>
        <Route  path ="/resume" component={Resume}/>
        <Route  path ="/projects" component={Project}/>
        <Route  path ="/contact" component={Contact}/>
    </Switch>
)
export default Main;
