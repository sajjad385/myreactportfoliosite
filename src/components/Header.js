import React,{Component} from 'react';

import {Content, Drawer, Header, Layout, Navigation} from "react-mdl";
import Main from "./Main";
import {Link} from "react-router-dom";

class NavComponent extends Component{

  render(){

      return (
      <div className="demo-big-content">
          <Layout>
          {/*<Layout style={{background: 'url(https://images.pexels.com/photos/574087/pexels-photo-574087.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940) center / cover'}}>*/}
              <Header className="header-color" title={<Link style={{textDecoration:'none', color:'white'}} to="/">MSHS</Link>} scroll>
                  <Navigation>
                      <Link to="/">Home</Link>
                      <Link to="/resume">Resume</Link>
                      <Link to="/projects">Projects</Link>
                      <Link to="/contact">Contact</Link>
                  </Navigation>
              </Header>
              <Drawer  title="MSHS">
                  <Navigation>
                      <Link to="/">Home</Link>
                      <Link to="/resume">Resume</Link>
                      <Link to="/projects">Projects</Link>
                      <Link to="/contact">Contact</Link>
                  </Navigation>
              </Drawer>
              <Content>
                  <div className="page-content" />
                  <Main/>
              </Content>
          </Layout>
      </div>

  );
  }

}
export default NavComponent;

