import React,{Component} from 'react';
import {Cell, Grid} from "react-mdl";

class Home extends Component{

    render(){
        return (
            <div style={{width:'100%',margin:'auto'}}>
                <Grid className="home-grid">
                    <Cell col={4}>
                        <img src="https://gitlab.com/uploads/-/system/user/avatar/2035473/avatar.png" alt="avator" className="avator-img"/>
                    </Cell>
                    <Cell col={8}>
                        <div className="banner-text">
                            <h1>Full Stack Developer</h1>
                            <hr/>
                            <p>HTML/CSS | Bootstrap | PHP | MySQL | JavaScript |  Laravel | React | Dart | Flutter </p>
                            <div className="social-links">
                                <a href="https://www.linkedin.com/in/sorad85/" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-linkedin-square"  aria-hidden="true"/>
                                </a>
                                <a href="https://gitlab.com/sajjad385" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-gitlab"  aria-hidden="true"/>
                                </a>
                                <a href="https://facebook.com/sorad85" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-facebook-square"  aria-hidden="true"/>
                                </a>
                                <a href="https://twitter.com/HSorad" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-twitter-square"  aria-hidden="true"/>
                                </a>
                            </div>
                        </div>
                    </Cell>
                </Grid>
            </div>

        );
    }

}
export default Home;

