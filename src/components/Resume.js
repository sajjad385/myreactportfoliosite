import React,{Component} from 'react';
import {Cell, Grid} from "react-mdl";
import Education from "./Education";
import Experience from "./Experience";
import Skills from "./Skills";
import Training from "./Training";

class Resume extends Component{

    render(){
        return (
            <div>
                <Grid>
                    <Cell col={4}>
                        <div style={{textAlign:'center'}}>
                            <img src="https://gitlab.com/uploads/-/system/user/avatar/2035473/avatar.png" alt="avator" style={{height:'200px', borderRadius:'50%'}}/>
                        </div>
                        <h2 className="resume-name">SAJJAD HOSSAIN</h2>
                        <h4 className="resume-designation">Full Stack Developer</h4>
                        <hr style={{borderTop:'3px solid #833fb2', width:'50%'}}/>
                        <p>Hi I'm Mohammad Sajjad Hossain technically web developer, I have multiple years of website design expertise behind me. can be a strong driving force for improving the performance,scalability and reliability of development projects. I have a long track record of creatingdynamic, rich and interesting web portals, and of providing long term solutions to front-end problems. I have experience of working in leading global internet technology companies and of completing challenging client projects. As a self directed and self managed person sheisalways pushing myself to deliver ongoing process improvements in anything she does. Right now I am looking for a suitable position and to join an enthusiastic team who enjoy what they do.</p>
                        <hr style={{borderTop:'3px solid #833fb2', width:'50%'}}/>
                        <h5>Address</h5>
                        <p>Niketon Bazar, Road-8, Dhaka-1212</p>
                        <h5>Phone</h5>
                        <p>+88 018 6713 1561</p>
                        <h5>Email</h5>
                        <p>cse.sajjad.hossain@gmail.com</p>
                        <h5>Website</h5>
                        <p>csesajjad.netlify.com</p>
                        <hr style={{borderTop:'3px solid #833fb2', width:'50%'}}/>
                    </Cell>
                    <Cell className="resume-right-col" col={8}>
                        <h3  className="heading-title"><i className="fa fa-university"></i> Educational Qualification</h3>
                        <Education startYear={2020} endYear={'Running'} subject="Computer Science & Engineering"  degree="B.Sc" schoolName={'Green University Of Bangladesh -(GUB)'} schoolDetails="Green University of Bangladesh (GUB), one of the leading private universities in Bangladesh, was founded in 2003 under the Private University Act 1992 with a vision to create a global higher education center of excellence. GUB offers students from all walks of life the advantages of an affordable, personalized education of global standard. As a modern, dynamic, and innovative institution for undergraduate and graduate students, GUB lays stress on quality education imparted by a galaxy of highly qualified, dynamic, dedicated, and well-experienced faculty members with global exposure."/>
                        <Education startYear={2015} endYear={2019} subject="Computer Technology"  degree="Diploma In Engineering" schoolName={'Bangladesh Sweden Polytechnic Institute'} schoolDetails=" Bangladesh Sweden Polytechnic Institute (BSPI) is one of the famous and well reputed technical educational institute in Bangladesh. It was established at Kaptai under Rangamati Hill Tracts in 1963 with the help of Sweden Government. The total campus area is 30.95 acre. BSPI is one of the Government institute under ministry of education through Directorate of technical education while the academic program is controlled by the Bangladesh Technical Education Board (BTEB). It offers 4-years (8 Semester) Diploma-in-Engineering courses. "/>
                        <Education startYear={2005} endYear={2015} subject="Humanities"  degree="Dakhil Certificate" schoolName={'Betagi Rahmania Jameul Ulum Madrasah'} schoolDetails="Betagi Rahmania Jameul Ulum Madrasah is one of the famous and well reputed madrasah in Bangladesh. It was established at Betagi, Rangunia, Chittgong.It is one of the Government institute under ministry of education through Directorate of Madrasah education while the academic program is controlled by the Bangladesh Madrasah Education Board . "/>
                        <hr style={{borderTop: '3px solid #e22947'}}/>
                        <h3 className="heading-title"><i className="fa fa-briefcase"></i> Experience</h3>
                        <Experience startYear={'October-2019'} endYear={'Present'} designation="Shonod Edutainment"  jobName={'Junior Software Engineer'} jobDescriptions="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at aut dolore eius harum labore magni modi nisi non optio quia quod, sequi, tenetur totam ullam velit, vero voluptatem voluptates! Lorem ipsum dolor sit amet, consectetur "/>
                        <Experience startYear={'February-2019'} endYear={'September-2019'} designation="Pondit.com"  jobName={'Web Developer'} jobDescriptions="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at aut dolore eius harum labore magni modi nisi non optio quia quod, sequi, tenetur totam ullam velit, vero voluptatem voluptates! Lorem ipsum dolor sit amet, consectetur "/>
                        <hr style={{borderTop: '3px solid #e22947'}}/>
						
						
                        <h3  className="heading-title"><i className="fa fa-graduation-cap"></i> Training</h3>
						
						<Training startYear={'15-April-2020'} endYear={'14-May-2020'} instituteName="The App Brewery" trainingName={'Introduction to Flutter Development Using Dart'} trainingDescriptions="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at aut dolore eius harum labore magni modi nisi non optio quia quod, sequi,tenetur totam ullam velit, vero voluptatem voluptates! Lorem ipsum dolor sit amet, consectetur "/>
                        <Training startYear={'April-2020'} endYear={'May-2020'} instituteName="Udemy" trainingName={'Project Management with PRINCE2 & PRINCE2 Agile'} trainingDescriptions="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at aut dolore eius harum labore magni modi nisi non optio quia quod, sequi,tenetur totam ullam velit, vero voluptatem voluptates! Lorem ipsum dolor sit amet, consectetur "/>
                        <Training startYear={'April-2020'} endYear={'May-2020'} instituteName="Udemy"  trainingName={'JavaScript, Bootstrap & PHP'} trainingDescriptions="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at aut dolore eius harum labore magni modi nisi non optio quia quod, sequi, tenetur totam ullam velit, vero voluptatem voluptates! Lorem ipsum dolor sit amet, consectetur "/>
                        
                        <Training startYear={'February-2019'} endYear={'April-2019'} instituteName="Pondit.com"  trainingName={'Web App Development Using Laravel'} trainingDescriptions="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at aut dolore eius harum labore magni modi nisi non optio quia quod, sequi, tenetur totam ullam velit, vero voluptatem voluptates! Lorem ipsum dolor sit amet, consectetur "/>
                        <Training startYear={'February-2018'} endYear={'July-2018'} instituteName="Pondit.com"  trainingName={'Web App Development Using PHP'} trainingDescriptions="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at aut dolore eius harum labore magni modi nisi non optio quia quod, sequi, tenetur totam ullam velit, vero voluptatem voluptates! Lorem ipsum dolor sit amet, consectetur "/>
                        <Training startYear={'July-2017'} endYear={'October-2017'} instituteName="Basis Institute Of Technology & Management (BITM)"  trainingName={'Web Design'} trainingDescriptions="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi at aut dolore eius harum labore magni modi nisi non optio quia quod, sequi, tenetur totam ullam velit, vero voluptatem voluptates! Lorem ipsum dolor sit amet, consectetur "/>



                        <hr style={{borderTop: '3px solid #e22947'}}/>
                        <h3  className="heading-title"><i className="fa fa-leanpub"></i> Skills</h3>
                        <Skills skill="Laravel" progress={90}/>
                        <Skills skill="React" progress={50}/>
                        <Skills skill="Dart" progress={60}/>
						  <Skills skill="Flutter" progress={30}/>
                        <Skills skill="Jquery" progress={70}/>
                        <Skills skill="JavaScript" progress={75}/>
                        <Skills skill="PHP" progress={80}/>
                        <Skills skill="Lumen" progress={60}/>
                        <Skills skill="CakePHP" progress={30}/>
                        <Skills skill="MySQL" progress={77}/>
                        <Skills skill="Oracle" progress={35}/>
                        <Skills skill="HTMl/CSS" progress={85}/>
                        <Skills skill="Adobe Photoshop" progress={85}/>
                        <Skills skill="Adobe Illustrator" progress={80}/>
                        <Skills skill="Adobe XD" progress={40}/>
                    </Cell>
                </Grid>
            </div>

        );
    }

}
export default Resume;

