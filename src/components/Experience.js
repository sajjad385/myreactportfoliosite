import React,{Component} from 'react';
import {Cell, Grid} from "react-mdl";

class Experience extends Component{

    render(){
        return (
            <div>
                <Grid>
                    <Cell col={4}>

                        <p  style={{marginTop:'0px', marginBottom:'0px'}}>{this.props.startYear}  -  {this.props.endYear}</p>
                        <p  style={{marginTop:'0px', marginBottom:'0px'}}>{this.props.designation}</p>

                    </Cell>
                    <Cell col={8}>
                        <h4 style={{marginTop:'0px'}}>
                            {this.props.jobName}
                        </h4>
                        <p>{this.props.jobDescriptions}</p>
                    </Cell>
                </Grid>
            </div>

        );
    }

}
export default Experience;

