import React,{Component} from 'react';
import {Cell, Grid, ProgressBar} from "react-mdl";

class Skills extends Component{

    render(){
        return (
            <div>
                <Grid>

                    <Cell col={2}>
                           <p style={{margin:'0px', paddingTop:'0px'}}> {this.props.skill}</p>
                    </Cell>
                    <Cell col={10}>
                        <ProgressBar style={{margin:'10px auto', width:'100%'}} progress={this.props.progress}  buffer={20}/>

                    </Cell>

                </Grid>
            </div>

        );
    }

}
export default Skills;

